const $ = require('jquery');
const address = require('address');
const axios = require('axios');
const net = require('net');
// SOCKET
const WebSocket = require('ws');

// Custom Modules
const agentApi = require('./agentAPI');
const configs = require('./configs');
let agent = require('./mobileAgent');

const ip = address.ip();
let mac = 'not available';

// Button MOVE TO triggered
$('.js-move-to-clicked').click(() => {
    agentApi.API.moveTo();
});

let peer = null;

const ws = new WebSocket(`ws://${configs.socketServerEndpoint}`, {
    origin: `http://${configs.socketServerEndpoint}`
});

ws.on('open', function open() {
    address.mac(function (err, addr) {

        mac = addr;
        agentApi.mac = mac;
        agentApi.ws = ws;

        const socketClient = {
            type: 'open',
            ip: ip,
            mac: mac,
            id: mac.split(':').join(''),
        };
        // send client details
        ws.send(JSON.stringify(socketClient));

        // https://gist.github.com/yetithefoot/7592580

        peer = new Peer(
            mac.split(':').join(''),
            {
                config: {'iceServers': [
                    {url:'stun:stun.l.google.com:19302'},
                    {
                        url: 'turn:192.158.29.39:3478?transport=udp',
                        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                        username: '28224511:1379330808'
                    },
                ]},
                host: configs.peerServerEndpoint,
                port: configs.peerServerPort,
                path: '/'
            }
        );

        // peer = new Peer({key: '8jjx46xwu3qbbj4i'});

        peer.on('open', function (id) {
            console.log(`Peer ID opened with id  = ${id}`);
        });

        peer.on('connection', function (conn) {
            conn.on('data', function (data) {

                agent = agentApi.API.unserializeAgent(data);
                console.log(`Agent received on peer connection. Agent = ${agent} `);// p2p on destination receive and reply
                agent.value += '|' + mac.split(':').join('').substring(9);
                agent.getHostTime();
                agent.getHostMac();

                conn.send(agentApi.API.serializeAgent(agent));
                const parsedDate = new Date(agent.hostTimes[Object.keys(agent.hostTimes).length - 1]);

                const displayLog = `<p>The agent visited this host at ${parsedDate}</p><p>With mac address: ${agent.hostMacs[Object.keys(agent.hostMacs).length - 1]}</p>`

                $('#hostLog').append(displayLog);
                $('.js-move-to-clicked').removeClass('disabled');

                ws.send(JSON.stringify({type: 'log', data: displayLog }));
            });
        });

        peer.on('error', function (err) {
            console.log(err);
        });
    });
});

ws.on('close', function close() {
    console.log('ws disconnected');
});

ws.on('message', function incoming(data, flags) {
    if (data !== '') {
        const dataParsed = JSON.parse(data);

        switch (dataParsed.type) {
            case 'signalData':
                // console.log('socket signal data!!');
                break;
            case 'open':
                // console.log('socket opened!!');
                break;
            case 'conn_request':
                console.log('conn request ', dataParsed);
                let conn = peer.connect(dataParsed.targetId);

                conn.on('error', function (err) {
                    console.log('ERROR = ', err);
                });

                conn.on('open', function () {
                    conn.send(agentApi.API.serializeAgent(agent)); // p2p on origin starting
                });

                conn.on('data', function (data) { // p2p on origin after response from destination
                    console.log('L93 Incoming data', data);
                    $('.js-move-to-clicked').addClass('disabled');
                    agent.local = false;
                    agent.value = data.value;
                });

                break;
            case 'start_agent':
                console.log('start_agent ', dataParsed);
                $('.js-move-to-clicked').removeClass('disabled');
                agent.local = true;
                agent.value = `start|${dataParsed.id.substring(9)}`;
                $('#js-agent').text(dataParsed.id.substring(9));
                break;
            default:
                console.log("Something went wrong evaluating " + dataParsed.type + ".");
        }
    }
});
