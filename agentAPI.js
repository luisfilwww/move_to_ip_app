const serialize = require('node-serialize');

const agentAPI = {
    mac: null,
    ws: null,

    moveTo() {
        const socketRequest = {
            type: 'get_destination',
            macId: mac.split(':').join(''),
        };
        // send client details wait for socket to p2p
        ws.send(JSON.stringify(socketRequest));
    },

    serializeAgent(agent) {
    return serialize.serialize(agent);
    },

    unserializeAgent(serializedAgent) {
        return serialize.unserialize(serializedAgent);
    }
};

exports.API = agentAPI;
