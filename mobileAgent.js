const address = require('address');

const Agent = {
    value: 'empty',
    local: false,
    hostTimes: {},
    hostMacs: {},

    getHostTime: function() {
        this.hostTimes[Object.keys(this.hostTimes).length] = new Date().getTime();
    },

    getHostMac: function () {
        const that = this;
        address.mac(function (err, addr) {
            that.hostMacs[Object.keys(that.hostMacs).length] = addr;
        });
    }
};

module.exports = Agent;
